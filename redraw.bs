--[[
	redraw.bs	
]]

	firstDraw=true

function main(x, y, p)

	if firstDraw then
		bs_rect(0, 0, bs_canvas_width(), bs_canvas_height(), 0, 0, 0, 0)
	end

	firstDraw=false
	return 0

end
