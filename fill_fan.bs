--[[
	fill_fan.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"first_distance", 0, 100, 0},
		{"no_base", 0, 1, 0}
	}

	pm.ja={
		"開始距離", "基点無効"
	}

	for i in ipairs(pm) do

		local eval=function(s, ...)
			assert(loadstring(string.format(s, ...)))()
		end

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		eval('param%d=setParam(pm[%d])', i, i)
		eval('pm[%d].v=bs_param%d()', i, i)

	end

	default_size=setParam(20, 0)
	use_base=setParam(true)

	bs_setmode(1)

	firstDraw=true

-- main --
function main(x, y, p)

	if firstDraw then
		rec={x=x, y=y}
		c=1
	end

	if not firstDraw then

		local distance=bs_distance(rec[c-1].x-x, rec[c-1].y-y)
		local interval=3

		if c > 500*pm[1].v/100 then
			interval=math.max(bs_width(), 3)
		end

		if distance < interval then
			return 0
		end

		local bsx, bsy=bs_base()

		if pm[2].v == 1 then
			bsx, bsy=-1, -1
		end

		local rad=bs_atan(x-rec[c-1].x, y-rec[c-1].y)
		local ddx, ddy=math.cos(rad), math.sin(rad)
		local coverX, coverY=2*ddx, 2*ddy

		local r, g, b=bs_fore()
		local a=bs_opaque()*255

		if bsx ~= -1 and bsy ~= -1 then
			bs_polygon(bsx, bsy)
			bs_polygon(bsx-coverX, bsy-coverY)
		else
			bs_polygon(rec[1].x, rec[1].y)
			bs_polygon(rec[1].x-coverX, rec[1].y-coverY)
		end

		bs_polygon(rec[c-1].x-coverX, rec[c-1].y-coverY)
		bs_polygon(x+coverX, y+coverY)
		bs_fill(r, g, b, a)

	end

	rec[c]={x=x, y=y}
	c=c+1

	firstDraw=false

	return 1

end
