--[[
	fill.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"size_p", 0, 100, 25},
		{"line", 0, 1, 1},
		{"no_base", 0, 1, 0}
	}

	pm.ja={
		"サイズ_p", "線を描画する", "基点無効"
	}

	for i in ipairs(pm) do

		local eval=function(s, ...)
			assert(loadstring(string.format(s, ...)))()
		end

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		eval('param%d=setParam(pm[%d])', i, i)
		eval('pm[%d].v=bs_param%d()', i, i)

	end

	default_size=setParam(10, 0)
	use_base=setParam(true)

	bs_setmode(1)

	firstDraw=true

function main(x, y, p)

	if firstDraw then
		rec={x=x, y=y}
		c=1
	end

	local pp=1

	if pm[1].v ~= 0 then
		pp=p^(4*pm[1].v/100)*(1-bs_width_min()/bs_width_max())+bs_width_min()/bs_width_max()
	end

	local w=bs_width_max()*pp

	if not firstDraw then

		local distance=bs_distance(rec[c-1].x-x, rec[c-1].y-y)
		local interval=w*(0.1+(0.4*(1-math.min(w, 5)/5)))

		if distance < interval then
			return 0
		end
	end

	local r, g, b=bs_fore()
	local a=bs_opaque()*255

	if pm[2].v == 1 then
		bs_ellipse(x, y, w, w*0.98, 0, r, g, b, a)
	end

	rec[c]={x=x, y=y}
	c=c+1

	firstDraw=false

	return 1

end

function last(x, y, p)

	local r, g, b=bs_fore()
	local a=bs_opaque()*255

	local bsx, bsy=bs_base()

	if pm[3].v == 1 then
		bsx, bsy=-1, -1
	end

	if bsx~=-1 and bsy~=-1 then
		bs_polygon(bsx, bsy)
	end

	for i, v in ipairs(rec) do
		bs_polygon(v.x, v.y)
	end

	bs_fill(r, g, b, a)

end
